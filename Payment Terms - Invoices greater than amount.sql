--identify the number of invoices that were above a certain limit.
with invoice_amount as
(
select  quarter, 
        invoice, 
        sum(amount) as grouped
from sale
where financial_year in ('FY2015','FY2016')
group by 1,2
)
select  b.quarter, 
        count(distinct a.invoice) as num_invoices, 
        count(distinct b.email_domain) as num_cust
from invoice_amount as a
left join sale as b on a.invoice = b.invoice and a.quarter = b.quarter
where a.grouped > 5000
and b.sold_via_partner = 'True'
group by 1;

--identify the amount of money from those with invoices that were above a certain limit.
with invoice_amount as
(
select  quarter, 
        invoice, 
        sum(amount) as grouped
from sale
where financial_year in ('FY2015','FY2016')
group by 1,2
),
cohort as 
(
select  b.quarter, 
        a.invoice,
        b.email_domain
from invoice_amount as a
left join sale as b on a.invoice = b.invoice and a.quarter = b.quarter
where a.grouped > 10000
--and b.sold_via_partner = 'True'
group by 1,2,3
)
select a.quarter, sum(b.amount)::money
from cohort as a
left join sale as b on a.invoice = b.invoice and a.quarter = b.quarter
where a.invoice = b.invoice
group by 1 


--identifying the number of customers on Cloud annual above a certain limit
with invoice_amount as
(
select  quarter, 
        invoice, 
        sum(amount) as grouped
from sale
where financial_year in ('FY2015','FY2016')
and hosted_annual = 'true'
group by 1,2
)
select  b.quarter, 
        count(distinct a.invoice) as num_invoices, 
        count(distinct b.email_domain) as num_cust
from invoice_amount as a
left join sale as b on a.invoice = b.invoice and a.quarter = b.quarter
where a.grouped > 5000
and b.sold_via_partner = 'True'
group by 1;
