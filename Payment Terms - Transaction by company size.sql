select a.quarter, 
case
 when company_size is null then 'unknown'
 when company_size<=10 then '1-10'
 when company_size between 11 and 50 then '11-50'
 when company_size between 51 and 500 then '51-500'
 when company_size > 500 then '500+'
end as company_size, 
sum(a.amount)
from sale as a
left join playground_bizops.customer_size as b on a.email_domain = b.email_domain
where financial_year > 'FY2013'
--and partner_id > 0
and amount >=10000
--and payment_method = 'PAIDCC' 
group by 1,2
order by 1,2

select a.quarter, 
case
 when company_size is null then 'unknown'
 when company_size<=10 then '1-10'
 when company_size between 11 and 50 then '11-50'
 when company_size between 51 and 500 then '51-500'
 when company_size > 500 then '500+'
end as company_size, 
count(distinct a.email_domain)
from sale as a
left join playground_bizops.customer_size as b on a.email_domain = b.email_domain
where financial_year > 'FY2013'
and partner_id > 0
and amount >=10000
group by 1,2
order by 1,2