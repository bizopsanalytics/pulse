with invoice_amount as
(
select  quarter, 
        invoice, 
        sum(amount) as grouped
from sale
where financial_year in ('FY2015','FY2016')
group by 1,2
),
cohort as 
(
select  b.quarter, 
        a.invoice,
        b.email_domain
from invoice_amount as a
left join sale as b on a.invoice = b.invoice and a.quarter = b.quarter
where a.grouped > 10000
--and b.sold_via_partner = 'True'
group by 1,2,3
)
select a.quarter, sum(b.amount)::money
from cohort as a
left join sale as b on a.invoice = b.invoice and a.quarter = b.quarter
where a.invoice = b.invoice
and b.payment_method = 'PAIDCC' 
group by 1 