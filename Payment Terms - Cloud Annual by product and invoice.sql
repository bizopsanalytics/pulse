
-- find the total number of cloud annual licenses sold each quarter.
select quarter, count(distinct email_domain)
from sale
where license_level = 'Full'
and financial_year > 'FY2014'
--and sold_via_partner = 'True'
and hosted_annual = 'true'
group by 1


--identifying the number of customers on Cloud annual above a certain limit
with invoice_amount as
(
select  quarter, 
        invoice, 
        sum(amount) as grouped
from sale
where financial_year in ('FY2015','FY2016')
and hosted_annual = 'true'
group by 1,2
)
select  a.invoice, 
        b.email_domain,
        b.sold_via_partner,
        b.base_product,
        b.hosted_annual,
        b.amount
from invoice_amount as a
left join sale as b on a.invoice = b.invoice and a.quarter = b.quarter
where a.grouped > 5000
group by 1,2,3,4,5,6;
