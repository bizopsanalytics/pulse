select month, count(distinct email_domain), count(email)
from public.sale
where sale_type = 'New to New'
and financial_year = 'FY2016'
group by 1

;

with first_purchase as
(
        select email_domain, email, min(date) as min_date
        from public.sale
        where sale_type = 'Starter'
        group by 1,2
)

select a.month, count(distinct a.email_domain), count(a.email)
from public.sale as a
left join first_purchase as b on a.email = b.email
where a.sale_type = 'Starter'
and a.financial_year = 'FY2016'
and a.date = b.min_date
group by 1
