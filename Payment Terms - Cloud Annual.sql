
-- find the total number of cloud annual licenses sold each quarter.
select quarter, count(distinct email_domain)
from sale
where license_level = 'Full'
and financial_year > 'FY2014'
--and sold_via_partner = 'True'
and hosted_annual = 'true'
group by 1


--identifying the number of customers on Cloud annual above a certain limit
with invoice_amount as
(
select  quarter, 
        invoice, 
        sum(amount) as grouped
from sale
where financial_year in ('FY2015','FY2016')
--and hosted_annual = 'true'
group by 1,2
)
select  b.quarter, 
        b.sold_via_partner,
        b.payment_method,
        count(distinct a.invoice) as num_invoices, 
        count(distinct b.email_domain) as num_cust
from invoice_amount as a
left join sale as b on a.invoice = b.invoice and a.quarter = b.quarter
--where a.grouped > 5000
group by 1,2,3;
