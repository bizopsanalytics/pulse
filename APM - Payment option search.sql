with calculate as
(
Select month_submitted, base_product, question_id, choices, other_choices, text_comment, count(sen) as num_responses
from ( 
select *
from playground_support.cloud_churn_explorer_response_details
where survey_id = 2247307
and level in ('Evaluation')
and region in ('AMER','APAC','EMEA')
and tenure in ('0-3 Months','4-6 Months','7-12 Months','13-24 Months','24+ Months')
and platform = 'Cloud'
and choices is not null

) as base

Where other_choices ILIKE '%Paypal%'
or text_comment ILIKE '%Paypal%'
or other_choices ILIKE '%Payment option%'
or text_comment ILIKE '%Payment option%'
or other_choices ILIKE '%Payment method%'
or text_comment ILIKE '%Payment method%'
or other_choices ILIKE '%JCB%'
or text_comment ILIKE '%JCB%'
or other_choices ILIKE '%currency%'
or text_comment ILIKE '%currency%'
group by month_submitted, base_product, question_id, choices, other_choices, text_comment
order by month_submitted, base_product, choices)
select a.*
from calculate as a 
group by 1,2,3,4,5,6,7
order by 1,7 desc