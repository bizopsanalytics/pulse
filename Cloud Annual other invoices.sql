
--Cloud annual invoices after cloud annual - email_domain level
with invoice_amount as
(
        select  quarter, 
                invoice,
                max(date) as max_date,
                sum(amount) as grouped
        from    public.sale
        where   financial_year in ('FY2015')
        and     hosted_annual = true
        group by 1,2
),
invoice_detail as 
(
        select  a.quarter,
                a.invoice, 
                a.max_date,
                b.email_domain,
                b.sold_via_partner,
                b.base_product,
                b.hosted_annual,
                b.amount
        from    invoice_amount as a
        left join public.sale as b on a.invoice = b.invoice
        where   a.grouped > 5000
        group by 1,2,3,4,5,6,7,8
),
all_invoice as
(
        select  a.email_domain, 
                a.invoice as cloud_annual_invoice,
                b.invoice as new_invoice,
                a.max_date as cloud_annual_date,
                b.date as new_date,
                b.amount
        from    invoice_detail as a
        left join public.sale as b on a.email_domain = b.email_domain
        where   --b.invoice not in (select invoice from invoice_detail)
                --and 
                b.date >= a.max_date
                and b.amount>0
)

select  email_domain, 
        cloud_annual_invoice, 
        new_invoice,
        cloud_annual_date,
        new_date, 
        sum(amount) as invoice_amount
        from all_invoice
        group by 1,2,3,4,5

;
--Cloud annual invoices after cloud annual - email_domain level
with invoice_amount as
(
        select  quarter, 
                invoice,
                max(date) as max_date,
                sum(amount) as grouped
        from    public.sale
        where   financial_year in ('FY2015')
        and     hosted_annual = true
        group by 1,2
),
invoice_detail as 
(
        select  a.quarter,
                a.invoice, 
                a.max_date,
                b.email_domain,
                b.email,
                b.sold_via_partner,
                b.base_product,
                b.hosted_annual,
                b.amount
        from    invoice_amount as a
        left join public.sale as b on a.invoice = b.invoice
        where   a.grouped > 5000
        group by 1,2,3,4,5,6,7,8,9
),
all_invoice as
(
        select  a.email_domain,
                a.email, 
                a.invoice as cloud_annual_invoice,
                b.invoice as new_invoice,
                a.max_date as cloud_annual_date,
                b.date as new_date,
                b.amount
        from    invoice_detail as a
        left join public.sale as b on a.email = b.email
        where   --b.invoice not in (select invoice from invoice_detail)
                --and 
                b.date >= a.max_date
                and b.amount>0
)

select  email_domain, 
        email,
        cloud_annual_invoice, 
        new_invoice,
        cloud_annual_date,
        new_date, 
        sum(amount) as invoice_amount
        from all_invoice
        group by 1,2,3,4,5,6
;
--Cloud annual invoices after cloud annual - email_domain + country level
with invoice_amount as
(
        select  quarter, 
                invoice,
                max(date) as max_date,
                sum(amount) as grouped
        from    public.sale
        where   financial_year in ('FY2015')
        and     hosted_annual = true
        group by 1,2
),
invoice_detail as 
(
        select  a.quarter,
                a.invoice, 
                a.max_date,
                b.email_domain,
                b.country,
                b.email,
                b.sold_via_partner,
                b.base_product,
                b.hosted_annual,
                b.amount
        from    invoice_amount as a
        left join public.sale as b on a.invoice = b.invoice
        where   a.grouped > 5000
        group by 1,2,3,4,5,6,7,8,9,10
),
all_invoice as
(
        select  a.email_domain,
                a.email, 
                a.country,
                a.invoice as cloud_annual_invoice,
                b.invoice as new_invoice,
                a.max_date as cloud_annual_date,
                b.date as new_date,
                b.amount
        from    invoice_detail as a
        left join public.sale as b on a.email_domain = b.email_domain and a.country = b.country
        where   --b.invoice not in (select invoice from invoice_detail)
                --and 
                b.date >= a.max_date
                and b.amount>0
)

select  email_domain, 
        country,
        email,
        cloud_annual_invoice, 
        new_invoice,
        cloud_annual_date,
        new_date, 
        sum(amount) as invoice_amount
        from all_invoice
        group by 1,2,3,4,5,6,7
