with invoice_amount as
(
select  quarter, 
        invoice, 
        sum(amount) as grouped
from sale
where financial_year in ('FY2015','FY2016')
--and hosted_annual = 'true'
group by 1,2
)
select  b.quarter, 
        b.sold_via_partner,
        b.payment_method,
        count(distinct a.invoice) as num_invoices, 
        count(distinct b.email_domain) as num_cust
from invoice_amount as a
left join sale as b on a.invoice = b.invoice and a.quarter = b.quarter
where a.grouped > 5000
and b.hosted_annual = 'True'
group by 1,2,3;