-- demographics of companies with invoices > certain limit.
with invoice_amount as
(
select  quarter, 
        invoice, 
        sum(amount) as grouped
from sale
where financial_year in ('FY2015','FY2016')
group by 1,2
),
cohort as 
(
select  b.quarter, 
        a.invoice,
        b.email_domain
from invoice_amount as a
left join sale as b on a.invoice = b.invoice and a.quarter = b.quarter
--where a.grouped > 5000
and license_level = 'Full'
and b.sold_via_partner = 
                                --'False' 
                                'True'
group by 1,2,3
), final_group as
(
select a.quarter, a.email_domain, sum(b.amount)::money as total
from cohort as a
left join sale as b on a.invoice = b.invoice and a.quarter = b.quarter
where a.invoice = b.invoice
group by 1,2
) 
select  a.quarter, 
        case
                when b.company_size is null then 'unknown'
                when b.company_size<=10 then '1-10'
                when b.company_size between 11 and 50 then '11-50'
                when b.company_size between 51 and 500 then '51-500'
                when b.company_size between 501 and 1000 then '501 - 1000'
                when b.company_size > 1000 then '1001+'
        end as company_size, 
        sum(a.total) as total_spend,
        count(distinct a.email_domain) as unique_cust,
        (sum(a.total)/count(distinct a.email_domain)) as average_inv
from final_group as a
left join playground_bizops.customer_size as b on a.email_domain = b.email_domain
group by 1,2